## PrintPress Zuul Gateway Microservice

Gateway microservice, handles routing so that API calls are handled by designated microservice(s) and static web content by the web-server microservice.
